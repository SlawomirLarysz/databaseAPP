package pl.dao;

import org.junit.jupiter.api.*;
import pl.configuration.JdbcConnectionManager;
import pl.configuration.PropertyReader;
import pl.configuration.TestUtil;
import pl.domain.Address;
import pl.domain.Person;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;


class PersonDaoJpaImplTest {

    private PersonDao personDao;
    private JdbcConnectionManager jdbcConnectionManager;

    @BeforeEach
    void setUp() throws Exception {
        personDao = new PersonDaoJpaImpl();
        jdbcConnectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        TestUtil.setUpDatabase(jdbcConnectionManager);
    }

    @AfterEach
    void tearDown() throws Exception {
        TestUtil.cleanUpDatabase(jdbcConnectionManager);
    }

    @Test
    void findAllTest(){
        //when
        List<Person> personList = personDao.findAll();
        Set<Integer> heightList = personList.stream().map(Person::getHeight).collect(Collectors.toSet());
        //then
        assertThat(personList).hasSize(3);
        assertThat(heightList).containsExactlyInAnyOrder(176, 165, 120);
    }

    @Test
    void findAllByValueAndSortedTest(){
        //when
        List<Person> findPersonByName = personDao.findAllByValueAndSorted("name", "desc", "Felipe");
        //then
        assertThat(findPersonByName.get(0).getName()).isEqualTo("Felipe");

        //when
        List<Person> findPersonBySurname = personDao.findAllByValueAndSorted("surname", "asc", "Wexler");
        //then
        assertThat(findPersonBySurname.get(0).getSurname()).isEqualTo("Wexler");

        //when
        List<Person> findPersonByCity = personDao.findAllByValueAndSorted("city", "desc", "Bytom");
        //then
        assertThat(findPersonByCity.get(0).getName()).isEqualTo("Felipe");

        //when
        List<Person> findPersonByStreet = personDao.findAllByValueAndSorted("street", "asc", "Falata");
        //then
        assertThat(findPersonByStreet.get(0).getAge()).isEqualTo(20);

        //when
        List<Person> wrongTypeOfSorted = personDao.findAllByValueAndSorted("street", "wrongTypeOfSorted", "Falata");
        //then
        assertThat(wrongTypeOfSorted).isNull();

        //when
        List<Person> wrongValue = personDao.findAllByValueAndSorted("wrongValue", "desc", "Bytom");
        //then
        assertThat(wrongValue).isNull();



    }

    @Test
    void findByIdTest() {
        //when
        Person person = findOneOfPersons();
        Optional<Person> optionalPerson = personDao.findById(person.getId());
        //then
        assertThat(optionalPerson).isPresent();
        assertThat(optionalPerson.get().getAge()).isEqualTo(20);


    }

    @Test
    void createTest(){
        //given
        Address address = new Address("Katowice", "42-099", "Chorzowska", 90, 30);
        Person person = new Person("Jan", "Nowak", 20, 199, 105.9, address);
        //when
        int id = personDao.create(person);
        Optional<Person> createdPerson = personDao.findById(id);
        //then
        assertThat(createdPerson).isPresent();
        assertThat(createdPerson.get().getAddress().getStreet()).isEqualTo("Chorzowska");

    }

    @Test
    void updateTest(){
        //given
        Person oneOfPersons = findOneOfPersons();
        oneOfPersons.setName("zmiana");
        //when
        int personId = personDao.update(oneOfPersons);
        Optional<Person> person = personDao.findById(personId);
        //then
        assertThat(person).isPresent();
        assertThat(person.get().getName()).isEqualTo("zmiana");
    }

    @Test
    void deleteTest() {
        //given
        Person oneOfPersons = findOneOfPersons();
        //when
        personDao.delete(oneOfPersons.getId());
        Optional<Person> person = personDao.findById(oneOfPersons.getId());
        //then
        assertThat(person).isNotPresent();
    }

    @Test
    void deleteAllTest() {
        //when
        personDao.deleteAll();
        final List<Person> persons = personDao.findAll();
        //then
        assertThat(persons).isEmpty();
    }





    private Person findOneOfPersons() {
        Optional<Person> firstPerson = personDao.findAll()
                .stream()
                .filter(x -> x.getAge() == 20)
                .findFirst();
        assertThat(firstPerson).isPresent();
        return firstPerson.get();
    }
}
