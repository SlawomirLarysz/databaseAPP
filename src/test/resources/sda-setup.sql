INSERT INTO person (name, surname, age, height, weight, city, zip_code, street, home_number, flat_number)
            VALUES ('Felipe', 'Wexler', '20', '176', '68.0', 'Bytom', '41-902', 'Falata', '20', '3'),
            ('Scott', 'Sowers', '70', '165', '56.9', 'Tarnowskie Gory', '42-600', 'Legionow', '15', '3'),
            ('Evelyn', 'Stanley', '10', '120', '15.5', 'Katowice', '41-031', 'Chorzowaska', '28', '52');