package pl;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URL;

public class Main extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);


    @Override
    public void start(Stage primaryStage) throws Exception {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if(resource!=null) {
            Parent root = FXMLLoader.load(resource);
            primaryStage.setTitle("People DataBase App");
            primaryStage.setScene(new Scene(root, 1040, 1000));
            primaryStage.setMaxWidth(1060.0);
            primaryStage.setMaxHeight(1000.0);
            primaryStage.setMinWidth(730.0);
            primaryStage.show();
        } else {
            LOGGER.error("Resource is null.");
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

