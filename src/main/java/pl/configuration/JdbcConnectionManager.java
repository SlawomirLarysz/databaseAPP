package pl.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConnectionManager {
    private final PropertyReader propertyReader;

    public JdbcConnectionManager(PropertyReader propertyReader) {
        this.propertyReader = propertyReader;
    }

    Connection getConnection() throws SQLException {
        String url = propertyReader.getUrl();
        String userName = propertyReader.getUserName();
        String password = propertyReader.getPassword();

        return DriverManager.getConnection(url, userName, password);
    }
}
