package pl.configuration;

import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

   private final String url;
   private final String userName;
   private final String password;

    private PropertyReader(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    String getUrl() {
        return url;
    }

    String getUserName() {
        return userName;
    }

    String getPassword() {
        return password;
    }

    public static PropertyReader loadConfiguration() throws IOException {
        Properties properties = new Properties();
        properties.load(PropertyReader.class.getClassLoader().getResourceAsStream("db.config.properties"));
        return new PropertyReader(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password"));
    }
}
