package pl.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Table (name = "address")
@Embeddable
public class Address implements Serializable {

    private String city;

    private String zipCode;

    private String street;

    private int homeNumber;

    private int flatNumber;

    public Address(String city, String zipCode, String street, int homeNumber, int flatNumber) {
        this.city = city;
        this.zipCode = zipCode;
        this.street = street;
        this.homeNumber = homeNumber;
        this.flatNumber = flatNumber;
    }

    public Address() {

    }

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Address setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public Address setStreet(String street) {
        this.street = street;
        return this;
    }

    public int getHomeNumber() {
        return homeNumber;
    }

    public Address setHomeNumber(int homeNumber) {
        this.homeNumber = homeNumber;
        return this;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public Address setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
        return this;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", street='" + street + '\'' +
                ", homeNumber=" + homeNumber +
                ", flatNumber=" + flatNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return homeNumber == address.homeNumber &&
                flatNumber == address.flatNumber &&
                Objects.equals(city, address.city) &&
                Objects.equals(zipCode, address.zipCode) &&
                Objects.equals(street, address.street);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, zipCode, street, homeNumber, flatNumber);
    }
}
