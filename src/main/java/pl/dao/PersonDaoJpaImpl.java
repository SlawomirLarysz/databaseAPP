package pl.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.domain.Person;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

public class PersonDaoJpaImpl implements PersonDao {

    private static final EntityManagerFactory emf;
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDaoJpaImpl.class);

    static {
        emf = Persistence.createEntityManagerFactory("people_DB");
    }

    @Override
    public List<Person> findAll() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createQuery("FROM Person", Person.class);
        List<Person> personList = query.getResultList();
        em.close();
        return personList;
    }

    @Override
        public List<Person> findAllByValueAndSorted(String sortedValue, String sortedTypeASCorDESC, String parameter) {
        EntityManager em = emf.createEntityManager();
        Query query = null;
        List personList = null;

        if (sortedTypeASCorDESC.toUpperCase().equals("ASC") || sortedTypeASCorDESC.toUpperCase().equals("DESC")) {

            switch (sortedValue) {
                case "name":
                    query = em.createQuery
                            ("FROM Person WHERE name = :value ORDER BY name " + sortedTypeASCorDESC);
                    query.setParameter("value", parameter);
                    personList = query.getResultList();
                    break;
                case "surname":
                    query = em.createQuery
                            ("FROM Person WHERE surname = :value ORDER BY surname " + sortedTypeASCorDESC);
                    query.setParameter("value", parameter);
                    personList = query.getResultList();
                    break;
                case "city":
                    query = em.createQuery
                            ("FROM Person WHERE city = :value ORDER BY city " + sortedTypeASCorDESC);
                    query.setParameter("value", parameter);
                    personList = query.getResultList();
                    break;
                case "street":
                    query = em.createQuery
                            ("FROM Person WHERE street = :value ORDER BY street " + sortedTypeASCorDESC);
                    query.setParameter("value", parameter);
                    personList = query.getResultList();
                    break;
                default:
                    System.out.println("Wrong sorted value. Please turn sorted value and try again");
                    break;
            }
        } else {
            System.out.println("Wrong type of sorted. Please turn type of sorted and try again");
        }

        em.close();

        return personList;
    }


    @Override
    public Optional<Person> findById(int id) {
        EntityManager em = emf.createEntityManager();
        Optional<Person> person = Optional.ofNullable(em.find(Person.class, id));
        em.close();
        return person;
    }

    @Override
    public int create(Person person) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.persist(person);
            transaction.commit();
        } catch (RuntimeException e) {
            if(transaction != null && transaction.isActive()){
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());

        }

        em.close();
        return person.getId();
    }

    @Override
    public int update(Person person) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.merge(person);
            transaction.commit();

        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()){
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        }
        em.close();
        return person.getId();
    }

    @Override
    public void delete(int id) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            Person person = em.find(Person.class, id);
            if (person != null) {
                em.remove(person);
            }
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        }
        em.close();
    }

    @Override
    public void deleteAll() {

        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createQuery("FROM Person", Person.class);
        List<Person> resultList = query.getResultList();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            resultList.forEach(em::remove);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        }

        em.close();
    }


}
