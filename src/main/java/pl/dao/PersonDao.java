package pl.dao;

import pl.domain.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDao {

    List<Person> findAll();

    List<Person> findAllByValueAndSorted(String sortedValue, String sortedTypeASCorDESC, String parameter);

    Optional<Person> findById(int id);

    int create (Person person);

    int update (Person person);

    void delete (int id);

    void deleteAll();


}
