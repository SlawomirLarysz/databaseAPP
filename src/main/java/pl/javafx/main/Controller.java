package pl.javafx.main;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {



    //menu
    @FXML
    Parent mainPane;




    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }




     public void loadCreateMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("createDB.fxml");
        if (resource != null) {
            try {
                Pane createDBPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(createDBPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadUpdateMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("updateDB.fxml");
        if (resource != null) {
            try {
                Pane updateDBPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(updateDBPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadDeleteMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("deleteDB.fxml");
        if (resource != null) {
            try {
                Pane deleteDBPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(deleteDBPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadDeleteAllMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("deleteAllDB.fxml");
        if (resource != null) {
            try {
                Pane deleteAllDBPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(deleteAllDBPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadFindByIdMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("findById.fxml");
        if (resource != null) {
            try {
                Pane findByIdPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(findByIdPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void loadBasicInfoMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("basicInfo.fxml");
        if (resource != null) {
            try {
                Pane basicInfoPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(basicInfoPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadFindPeopleAndSortedMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("findPeopleAndSorted.fxml");
        if (resource != null) {
            try {
                Pane fidPeopleAndSortedPane = FXMLLoader.load(resource);
                Stage stage = (Stage) mainPane.getScene().getWindow();
                stage.setScene(new Scene(fidPeopleAndSortedPane, 1000, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}