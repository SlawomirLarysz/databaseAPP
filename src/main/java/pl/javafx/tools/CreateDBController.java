package pl.javafx.tools;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dao.PersonDao;
import pl.dao.PersonDaoJpaImpl;
import pl.domain.Address;
import pl.domain.Person;
import pl.javafx.dialogs.DialogsTools;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateDBController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDBController.class);

    @FXML
    Parent createDBPane;

    @FXML
    private TextField personNameTextField;
    @FXML
    private TextField personSurnameTextField;
    @FXML
    private TextField personAgeTextField;
    @FXML
    private TextField personHeightTextField;
    @FXML
    private TextField personWeightTextField;

    @FXML
    private TextField addressCityTextField;
    @FXML
    private TextField addressZipCodeTextField;
    @FXML
    private TextField addressStreetTextField;
    @FXML
    private TextField addressHomeNumberTextField;
    @FXML
    private TextField addressFlatNumberTextField;

    @FXML
    private TextArea textArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    public void backToMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) createDBPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

    public void createPersonClick(MouseEvent mouseEvent) {
        Person person = new Person();
        Address address = new Address();

        try {
            address.setCity(addressCityTextField.getText());
            address.setZipCode(addressZipCodeTextField.getText());
            address.setStreet(addressStreetTextField.getText());
            address.setHomeNumber(Integer.parseInt(addressHomeNumberTextField.getText()));
            address.setFlatNumber(Integer.parseInt(addressFlatNumberTextField.getText()));

            person.setName(personNameTextField.getText());
            person.setSurname(personSurnameTextField.getText());
            person.setAge(Integer.parseInt(personAgeTextField.getText()));
            person.setHeight(Integer.parseInt(personHeightTextField.getText()));
            person.setWeight(Double.parseDouble(personWeightTextField.getText()));
            person.setAddress(address);



            PersonDao personDao = new PersonDaoJpaImpl();
            int createResult = personDao.create(person);

            if (createResult != 0) {
                DialogsTools.informationCreate();
            }

        } catch (Exception e) {
            DialogsTools.exceptionInfo();
        }

    }
}
