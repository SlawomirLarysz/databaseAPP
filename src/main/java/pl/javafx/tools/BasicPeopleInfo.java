package pl.javafx.tools;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.dao.PersonDao;
import pl.dao.PersonDaoJpaImpl;
import pl.domain.Address;
import pl.domain.Person;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BasicPeopleInfo implements Initializable {



    @FXML
    Parent basicInfoPane;

    @FXML
    private TableView<Person> personTableView;
    @FXML
    private TableColumn<Person, Integer> idColumn;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private TableColumn<Person, String> surnameColumn;
    @FXML
    private TableColumn<Person, Integer> ageColumn;
    @FXML
    private TableColumn<Person, Integer> heightColumn;
    @FXML
    private TableColumn<Person, Double> weightColumn;

    @FXML
    private TableView<Address> addressTableView;
    @FXML
    private TableColumn<Address, String> zipCodeColumn;
    @FXML
    private TableColumn<Address, String> cityColumn;
    @FXML
    private TableColumn<Address, String> streetColumn;
    @FXML
    private TableColumn<Address, Integer> homeNumberColumn;
    @FXML
    private TableColumn<Address, Integer> flatNumberColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        PersonDao personDao = new PersonDaoJpaImpl();
        ObservableList<Person> observableListPerson = FXCollections.observableArrayList();
        ObservableList<Address> observableListAddress = FXCollections.observableArrayList();


        List<Person> personList = personDao.findAll();

        for (Person iterator : personList ) {
            observableListPerson.add(iterator);
            observableListAddress.add(iterator.getAddress());
        }

        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        heightColumn.setCellValueFactory(new PropertyValueFactory<>("height"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        personTableView.setItems(observableListPerson);


        zipCodeColumn.setCellValueFactory(new PropertyValueFactory<>("zipCode"));
        cityColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
        streetColumn.setCellValueFactory(new PropertyValueFactory<>("street"));
        homeNumberColumn.setCellValueFactory(new PropertyValueFactory<>("homeNumber"));
        flatNumberColumn.setCellValueFactory(new PropertyValueFactory<>("flatNumber"));
        addressTableView.setItems(observableListAddress);
    }

    public void backToMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage= (Stage) basicInfoPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
