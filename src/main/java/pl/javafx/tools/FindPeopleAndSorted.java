package pl.javafx.tools;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.dao.PersonDao;
import pl.dao.PersonDaoJpaImpl;
import pl.domain.Address;
import pl.domain.Person;
import pl.javafx.dialogs.DialogsTools;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class FindPeopleAndSorted implements Initializable {

    private String value;
    private String sortedTypeASCorDESC;

    @FXML
    Parent findAndSortedPane;

    @FXML
    private CheckBox nameCheckBox;
    @FXML
    private CheckBox surnameCheckBox;
    @FXML
    private CheckBox cityCheckBox;
    @FXML
    private CheckBox streetCheckBox;

    @FXML
    private CheckBox ascendingCheckBox;
    @FXML
    private CheckBox descendingCheckBox;

    @FXML
    private TextField parameterTextField;

    @FXML
    private TableView<Person> personTableView;
    @FXML
    private TableColumn<Person, Integer> idColumn;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private TableColumn<Person, String> surnameColumn;
    @FXML
    private TableColumn<Person, Integer> ageColumn;
    @FXML
    private TableColumn<Person, Integer> heightColumn;
    @FXML
    private TableColumn<Person, Double> weightColumn;

    @FXML
    private TableView<Address> addressTableView;
    @FXML
    private TableColumn<Address, String> zipCodeColumn;
    @FXML
    private TableColumn<Address, String> cityColumn;
    @FXML
    private TableColumn<Address, String> streetColumn;
    @FXML
    private TableColumn<Address, Integer> homeNumberColumn;
    @FXML
    private TableColumn<Address, Integer> flatNumberColumn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void backToMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage= (Stage) findAndSortedPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String checkSortedCategory(ActionEvent actionEvent) {
        if (nameCheckBox.isSelected() && !surnameCheckBox.isSelected() && !cityCheckBox.isSelected() && !streetCheckBox.isSelected()) {
            value = "name";
        } else if (!nameCheckBox.isSelected() && surnameCheckBox.isSelected() && !cityCheckBox.isSelected() && !streetCheckBox.isSelected()) {
            value = "surname";
        } else if (!nameCheckBox.isSelected() && !surnameCheckBox.isSelected() && cityCheckBox.isSelected() && !streetCheckBox.isSelected()) {
            value = "city";
        } else if (!nameCheckBox.isSelected() && !surnameCheckBox.isSelected() && !cityCheckBox.isSelected() && streetCheckBox.isSelected()) {
            value = "street";
        } else {
            value = "WRONG";
        }

        return value;
    }

    public String checkSortedMethod(ActionEvent actionEvent) {
        if (ascendingCheckBox.isSelected() && !descendingCheckBox.isSelected()) {
            sortedTypeASCorDESC = "ASC";
        } else if (!ascendingCheckBox.isSelected() && descendingCheckBox.isSelected()) {
            sortedTypeASCorDESC = "DESC";
        } else {
            sortedTypeASCorDESC = "WRONG";
        }

        return sortedTypeASCorDESC;
    }


    public void findPeopleAndSorted(MouseEvent mouseEvent) {

        if (value != "WRONG" && sortedTypeASCorDESC != "WRONG") {
            PersonDao personDao = new PersonDaoJpaImpl();

            try {
                List<Person> people = personDao.findAllByValueAndSorted(value, sortedTypeASCorDESC, parameterTextField.getText());
                System.out.println(people.size());

                if (people.size() == 0) {
                    DialogsTools.informationNotFoundPerson();

                } else {
                    ObservableList<Person> observableListPerson = FXCollections.observableArrayList();
                    ObservableList<Address> observableListAddress = FXCollections.observableArrayList();

                    for (Person iterator : people) {
                        observableListPerson.add(iterator);
                        observableListAddress.add(iterator.getAddress());
                    }
                    idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
                    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
                    surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
                    ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
                    heightColumn.setCellValueFactory(new PropertyValueFactory<>("height"));
                    weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
                    personTableView.setItems(observableListPerson);

                    zipCodeColumn.setCellValueFactory(new PropertyValueFactory<>("zipCode"));
                    cityColumn.setCellValueFactory(new PropertyValueFactory<>("city"));
                    streetColumn.setCellValueFactory(new PropertyValueFactory<>("street"));
                    homeNumberColumn.setCellValueFactory(new PropertyValueFactory<>("homeNumber"));
                    flatNumberColumn.setCellValueFactory(new PropertyValueFactory<>("flatNumber"));
                    addressTableView.setItems(observableListAddress);
                }

            } catch (Exception e) {
                DialogsTools.exceptionInfo();
            }

        } else {
            DialogsTools.informationWrongChoice();
            System.out.println("Wrong choice. Please try select again");
        }
    }
}
