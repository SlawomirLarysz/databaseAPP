package pl.javafx.tools;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.dao.PersonDao;
import pl.dao.PersonDaoJpaImpl;
import pl.javafx.dialogs.DialogsTools;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DeleteDBController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteDBController.class);

    @FXML
    Parent deleteDBPane;

//    @FXML
    @FXML
    private TextField personIdTextField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void backToMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) deleteDBPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void deletePersonClick(MouseEvent mouseEvent) {
        PersonDao personDao = new PersonDaoJpaImpl();

        try {
            int id = Integer.parseInt(personIdTextField.getText());

            if (personDao.findById(id).isPresent()) {
                personDao.delete(id);
                DialogsTools.informationDeleted();
            } else {
                DialogsTools.informationNotFoundPerson();
            }
        } catch (RuntimeException e) {
            DialogsTools.exceptionInfo();
        }

    }
}
