package pl.javafx.tools;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pl.dao.PersonDao;
import pl.dao.PersonDaoJpaImpl;
import pl.javafx.dialogs.DialogsTools;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class DeleteAllDBController implements Initializable {


    @FXML
    Parent deleteAllDBPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void backToMenu(MouseEvent mouseEvent) {
        URL resource = getClass().getClassLoader().getResource("main.fxml");
        if (resource != null) {
            try {
                Pane mainPane = FXMLLoader.load(resource);
                Stage stage = (Stage) deleteAllDBPane.getScene().getWindow();
                stage.setScene(new Scene(mainPane, 1040, 1000));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void deleteAllPersonClick(MouseEvent mouseEvent) {
        PersonDao personDao = new PersonDaoJpaImpl();

        try {
            personDao.deleteAll();
            DialogsTools.informationDeletedAll();
        } catch (Exception e) {
            DialogsTools.exceptionInfo();
        }


    }
}
